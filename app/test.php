<?php
require_once('change.php');
function change_test($answer,$source,$target,$amount){
    echo "source = $source, target = $target,amount = $amount, answer = $answer";

    if($answer == change($source,$target,$amount))
    {
        echo 'success<br>';
    }
    else
    {
        echo 'fail<br>';
    }
}

$test_input = [["answer"=> '$100.00' ,"source"=> 'TWD' ,"target"=> 'TWD' ,"amount"=> '$100'],
               ["answer"=> '$733.80' ,"source"=> 'TWD' ,"target"=> 'JPY' ,"amount"=> '$200'],
               ["answer"=> '$9.84' ,"source"=> 'TWD' ,"target"=> 'USD' ,"amount"=> '$300'],
               ["answer"=> '$107.82' ,"source"=> 'JPY' ,"target"=> 'TWD' ,"amount"=> '$107.82'],
               ["answer"=> '$500.00' ,"source"=> 'JPY' ,"target"=> 'JPY' ,"amount"=> '$500'],
               ["answer"=> '$9.74' ,"source"=> 'JPY' ,"target"=> 'USD' ,"amount"=> '$1,100'],
               ["answer"=> '$36,532.80' ,"source"=> 'USD' ,"target"=> 'TWD' ,"amount"=> '$1,200'],
               ["answer"=> '$145,341.30' ,"source"=> 'USD' ,"target"=> 'JPY' ,"amount"=> '$1,300'],
               ["answer"=> '$1,400.00' ,"source"=> 'USD' ,"target"=> 'USD' ,"amount"=> '$1,400']];
foreach($test_input as $input)
{
    change_test($input['answer'],$input['source'],$input['target'],$input['amount']);
}
