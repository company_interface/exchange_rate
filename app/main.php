<?php
require_once('change.php');

$source = $_GET['source'] ?? 'TWD';
$target = $_GET['target'] ?? 'TWD';
$amount = $_GET['amount'] ?? '0';
$result['msg'] =  'success';
$result['amount'] = change($source,$target,$amount);
echo json_encode($result);