<?php
function change($source,$target,$amount){
    $exchange_rate = ["TWD"=>["TWD"=>1,"JPY"=>3.669,"USD"=>0.03281],
                    "JPY"=>["TWD"=>0.26956,"JPY"=>1,"USD"=>0.00885],
                    "USD"=>["TWD"=>30.444,"JPY"=>111.801,"USD"=>1]];

    $amount = intval(preg_replace('/[^\d]/','',$amount));
    $change_amount =  $amount * $exchange_rate[$source][$target];
    return '$'.number_format($change_amount,2);
}
